<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::disableForeignKeyConstraints(); // Disable foreign key checks while altering table

        $latestId = DB::table('insured_data')->orderBy('id', 'desc')->first()->id;
        $sequenceName = 'insured_data_id_seq';
        $startId = $latestId + 1;
        DB::statement("ALTER SEQUENCE $sequenceName RESTART WITH $startId");

        Schema::enableForeignKeyConstraints(); // Re-enable foreign key checks
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
