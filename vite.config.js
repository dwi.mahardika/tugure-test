import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    optimizeDeps: {
        include: ['jquery', 'bootstrap'],
      },
    plugins: [
        laravel({
            input: [
                './resources/css/signin.css',
                './resources/sass/app.scss',
                './resources/js/app.js',
                './resources/js/insured-data.js'
            ],
            refresh: true,
        }),
    ],
});
