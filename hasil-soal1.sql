select 
    i.no_nota, 
    i.installment_ke, 
    nd.cob, 
    round(i.nilai_installment * nd.nilai_per_cob / n.nilai_nota, 2) as nilai
from 
    tbl_installment i
join 
    tbl_nota n on i.no_nota = n.no_nota
join 
    tbl_notadetail nd on i.no_nota = nd.no_nota
order by 
    i.no_nota, i.installment_ke, nd.id;