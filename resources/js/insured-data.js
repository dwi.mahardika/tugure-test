import $ from 'jquery';
import DataTable from 'datatables.net-bs5';
import 'datatables.net-buttons-bs5';
import jszip from 'jszip';
import pdfmake from 'pdfmake/build/pdfmake';
var pdfFonts = require('pdfmake/build/vfs_fonts.js');
import 'datatables.net-buttons/js/buttons.html5.js';
import 'datatables.net-buttons/js/buttons.print.js';
import '@fortawesome/fontawesome-free/css/all.min.css';
import Swal from 'sweetalert2';


pdfMake.vfs = pdfFonts.pdfMake.vfs;

window.JSZip = jszip;

document.addEventListener('DOMContentLoaded', function () {

    const table = $('#dataTable').DataTable({
        dom: 'Bfrtip', // Ensure the buttons are shown
        buttons: ['copy', 'excel', 'pdf'],
        pagingType: 'full_numbers',
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: '/insured-data/search',
            type: 'GET',
            data: function (d) {
                d.page = 1;
                d.length = 10;
                d.order = [{ column: 0, dir: 'desc' }];
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            }
        },
        columns: [
            { data: 'id', name: 'id', visible: false },
            {
                data: null,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
                orderable: false, searchable: false,
            },
            { data: 'ktp', name: 'ktp' },
            { data: 'name', name: 'name' },
            {
                data: 'sex', name: 'sex',
                render: function (data, type, row, meta) {
                    var jk = ''
                    switch (data) {
                        case 'M':
                            jk = 'Male';
                            break;
                        case 'U':
                            jk = 'Unidentified';
                            break;
                        case 'F':
                            jk = 'Female';
                            break;
                    }

                    return jk;
                }
            },
            { data: 'birth_of_date', name: 'birth_of_date' },
            {
                data: null,
                render: function (data, type, row, meta) {
                    return `
                    <button type="button" class="btn btn-sm btn-success btnEdit"><i class="fas fa-edit"></i></button>
                    <button class='btn btn-sm btn-danger btnDel'><i class="fas fa-trash"></button>`;
                }
            },
        ]
    });

    table.on('click', 'tbody tr .btnDel', function (e) {
        e.preventDefault();
        var rowData = table.row($(this).closest('tr')).data();
        var ID = rowData.id;
        console.log(ID)  
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!"
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/insured-data/delete/' + ID,
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    success: function(response) {
                        console.log(response);
                        Swal.fire({
                            title: "Deleted!",
                            text: "data has been deleted.",
                            icon: "success"
                        });
                        window.location.href = '/';
                    },
                    error: function(xhr, status, error) {
                        Swal.fire({
                            title: "Failed!",
                            text: "data failed to delete.",
                            icon: "danger"
                        });
                        console.error(error + " "+ ID);
                    }
                });  
            }
        });
    });

    table.on('click', 'tbody tr .btnEdit', function (e) {
        e.preventDefault();
        var rowData = table.row($(this).closest('tr')).data();
        var ID = rowData.id;
        var data = $.ajax({
            url: '/insured-data/get/' + ID,
            type: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success: function(resp) {
                resp.ktp = (resp.ktp === null) ? '' : resp.ktp;
                Swal.fire({
                    title: 'Edit Data',
                    html:
                        `
                        <style>
                        div:where(.swal2-container) .swal2-html-container {
                            text-align: left;
                        }
                        </style>
                        <form id="editForm" class="row g-3">
                        <input type="hidden" value="${resp.id}" name="id">
                        <div class="col-md-12">
                            <label for="editName" class="form-label" style="text-align:left">Name</label>
                            <input type="text" class="form-control" id="editName" name="name" value="${resp.name}">
                        </div>
                        <div class="col-md-12">
                            <label for="editKTP" class="form-label" style="text-align:left">KTP</label>
                            <input type="text" class="form-control" id="editKTP" name="ktp" value="${resp.ktp}">
                        </div>
                        <div class="col-md-12">
                            <label for="editGender" class="form-label" style="text-align:left">Gender</label>
                            <select class="form-select" id="editGender" name="sex">
                                <option value="M" ${resp.sex === 'M' ? 'selected' : ''}>Male</option>
                                <option value="F" ${resp.sex === 'F' ? 'selected' : ''}>Female</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="editDateOfBirth" class="form-label" style="text-align:left">Date of Birth</label>
                            <input type="date" class="form-control" id="editDateOfBirth" name="date_of_birth" value="${resp.birth_of_date}">
                        </div>
                    </form>`,
                    showCancelButton: true,
                    confirmButtonText: 'Save',
                    cancelButtonText: 'Cancel',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        // Handle form submission
                        var formData = new FormData($('#editForm')[0]);
                        return $.ajax({
                            url: '/insured-data/update',
                            type: 'POST', // Change to POST
                            data: formData,
                            processData: false,
                            contentType: false,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            }
                        }).then(response => {
                            if (!response.success) {
                                throw new Error(response.message || 'Update failed');
                            }
                            return response;
                        }).catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            );
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then(result => {
                    if (result.isConfirmed) {
                        Swal.fire({
                            title: 'Success',
                            text: 'Data updated successfully',
                            icon: 'success'
                        });
                        // Reload data table or refresh page
                        // For example, you can reload the data table:
                        table.ajax.reload();
                    }
                });
            },
            error: function(xhr, status, error) {
                console.error(error);
                Swal.fire({
                    title: 'Error',
                    text: 'Failed to fetch data for editing',
                    icon: 'error'
                });
            }
        });  
        
    });
});