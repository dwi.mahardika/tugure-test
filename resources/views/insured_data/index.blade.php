<x-app-layout>
    @vite(['resources/js/insured-data.js'])

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container px-4 py-5" id="hanging-icons">
                    @if (!isset($user))
                        <h2 class="pb-2 border-bottom">Welcome, please register or login first</h2>
                    @else
                        <h2 class="pb-2 border-bottom">Insured Data</h2>
                        <div class="row g-4 py-5 row-cols-1 row-cols-lg-12">
                            <button class="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#formModal">Tambah
                                Data</button>
                            <table id="dataTable" class="table table-striped display compact" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>No</th>
                                        <th>KTP</th>
                                        <th>Name</th>
                                        <th>Gender</th>
                                        <th>Birth of Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="formModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="staticBackdropLabel">Input</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" action="{{ route('add.insured.data') }}" class="row g-3">
                    <div class="modal-body">
                        @csrf
                        <div class="modal-body">
                            <div class="mb-3 row">
                                <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputName" name="name">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="inputKTP" class="col-sm-2 col-form-label">KTP</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputKTP" name="ktp">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="inputGender" class="col-sm-2 col-form-label">Sex</label>
                                <div class="col-sm-10">
                                    <select class="form-select" aria-label="input gender" name="sex">
                                        <option value="U" selected>-- Select Sex --</option>
                                        <option value="M">Male</option>
                                        <option value="F">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="inputTglLahir" class="col-sm-2 col-form-label">Birth of Date</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control" id="inputTglLahir" name="birth_of_date">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary closeModal"
                                data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
