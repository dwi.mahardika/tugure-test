<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\InsuredDataController;
use Illuminate\Support\Facades\Route;

Route::get('/', [InsuredDataController::class, 'index'])->name('index');

Route::middleware('auth')->group(function () {
    Route::get('/insured-data/get/{id}', [InsuredDataController::class, 'get'])->name('get.insured.data');
    Route::get('/insured-data/search', [InsuredDataController::class, 'search'])->name('search.insured.data');
    Route::post('/insured-data/add', [InsuredDataController::class, 'store'])->name('add.insured.data');
    Route::post('/insured-data/update', [InsuredDataController::class, 'update'])->name('update.insured.data');
    Route::delete('/insured-data/delete/{id}', [InsuredDataController::class, 'delete'])->name('delete.insured.data');
});

require __DIR__.'/auth.php';
