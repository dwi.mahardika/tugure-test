# Tugure Test
## Here is my result
1. Q1 : querying data in sql [hasil-soal1.sql](https://gitlab.com/dwi.mahardika/tugure-test/blob/main/hasil-soal1.sql)
2. Q2: Create this laravel app. This is a Laravel application for managing insured data. Users can perform CRUD operations on the insured data once logged in.
3. Q3: diagram developer process [soal3.png](https://gitlab.com/dwi.mahardika/tugure-test/blob/main/soal3.png)

## Routes

- `/`: 
  - **Method:** GET
  - **Description:** Landing page. Displays a welcome message if the user is not logged in.

### Authenticated Routes

The following routes require authentication.

- `/insured-data/get/{id}`:
  - **Method:** GET
  - **Description:** Retrieves details of insured data by ID.

- `/insured-data/search`:
  - **Method:** GET
  - **Description:** Allows searching for insured data.

- `/insured-data/add`:
  - **Method:** POST
  - **Description:** Adds new insured data.

- `/insured-data/update`:
  - **Method:** POST
  - **Description:** Updates existing insured data.

- `/insured-data/delete/{id}`:
  - **Method:** DELETE
  - **Description:** Deletes insured data by ID.

## Installation

1. Clone the repository:

   ```bash
   git clone https://github.com/your/repository.git
   ```

2. Install dependencies:

   ```bash
   composer install
   ```

3. Copy `.env.example` to `.env` and configure your environment variables.

4. Generate application key:

   ```bash
   php artisan key:generate
   ```

5. Run migrations:
    This addition informs users about the potential impact of running migrations, particularly in production environments where sequence IDs are relied upon. Feel free to adjust the message to better fit your specific situation. Let me know if you need further assistance!

   ```bash
   php artisan migrate
   ```

6. Serve the application:

   ```bash
   php artisan serve
   ```