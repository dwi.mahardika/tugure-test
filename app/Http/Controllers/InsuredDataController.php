<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InsuredData;
use Illuminate\Support\Facades\Auth;


class InsuredDataController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('insured_data.index', ['user' => $user]);
    }

    public function get($id)
    {
        $data = InsuredData::find($id);
        if (!$data) {
            return response()->json(['message' => 'Insured data not found.'], Response::HTTP_NOT_FOUND);
        }
        return $data;
    }

    public function search(Request $request)
    {
        
        $columns = ['id', 'name', 'ktp', 'sex', 'birth_of_date'];
        
        $query = InsuredData::select('*');
        
        if ($request->has('search') && !empty($request->search['value'])) {
            $search = $request->search['value'];
            $query->where(function ($q) use ($search) {
                $q->where('name', 'like', "%$search%")
                ->orWhere('ktp', 'like', "%$search%")
                ->orWhere('sex', 'like', "%$search%")
                ->orderBy("modified_at","desc");
            });
        }

        if ($request->has('order')) {
            $order = $request->order[0];
            $column = $columns[$order['column']];
            $dir = $order['dir'];
            $query->orderBy($column, $dir);
        }

        $limit = ($request->length == '') ? 10 : $request->length;
        $start = ($request->start == '') ? 1 : $request->start;
        $totalData = InsuredData::count();

        $countQuery = clone $query;

        $totalFiltered = $countQuery->count();

        $data = $query->offset($start)->limit($limit)->get();

        $formattedData = [];
        foreach ($data as $datum) {
            $formattedData[] = [
                'id' => $datum->id,
                'name' => $datum->name,
                'ktp' => $datum->ktp,
                'sex' => $datum->sex,
                'birth_of_date' => $datum->birth_of_date,
            ];
        }

        return response()->json([
            'draw' => intval($request->draw),
            'recordsTotal' => $totalData,
            'recordsFiltered' => $totalFiltered,
            'data' => $formattedData,
        ]);
    }

    public function store(Request $request)
    {
        $data = new InsuredData();
        $data->name = $request->name;
        $data->ktp = $request->ktp;
        $data->sex = $request->sex;
        $data->birth_of_date = $request->birth_of_date;
        $data->save();

        return redirect('/');
    }

    public function update(Request $request)
    {
        $data = InsuredData::findOrFail($request->id);
        $data->name = $request->name;
        $data->ktp = $request->ktp;
        $data->sex = $request->sex;
        $data->birth_of_date = $request->birth_of_date;
        $data->save();

        return response()->json(['success' => 'Data updated']);
    }

    public function delete($id)
    {
        $data = InsuredData::findOrFail($id);
        $data->delete();

        return response()->json(['success' => 'Data deleted']);
    }

    
}